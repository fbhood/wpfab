<?php

/**
 * Registers the `album` taxonomy,
 * for use with 'photo'.
 */
function album_init() {
	register_taxonomy( 'album', array( 'photo' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Albums', 'wpfab' ),
			'singular_name'              => _x( 'Album', 'taxonomy general name', 'wpfab' ),
			'search_items'               => __( 'Search Albums', 'wpfab' ),
			'popular_items'              => __( 'Popular Albums', 'wpfab' ),
			'all_items'                  => __( 'All Albums', 'wpfab' ),
			'parent_item'                => __( 'Parent Album', 'wpfab' ),
			'parent_item_colon'          => __( 'Parent Album:', 'wpfab' ),
			'edit_item'                  => __( 'Edit Album', 'wpfab' ),
			'update_item'                => __( 'Update Album', 'wpfab' ),
			'view_item'                  => __( 'View Album', 'wpfab' ),
			'add_new_item'               => __( 'Add New Album', 'wpfab' ),
			'new_item_name'              => __( 'New Album', 'wpfab' ),
			'separate_items_with_commas' => __( 'Separate Albums with commas', 'wpfab' ),
			'add_or_remove_items'        => __( 'Add or remove Albums', 'wpfab' ),
			'choose_from_most_used'      => __( 'Choose from the most used Albums', 'wpfab' ),
			'not_found'                  => __( 'No Albums found.', 'wpfab' ),
			'no_terms'                   => __( 'No Albums', 'wpfab' ),
			'menu_name'                  => __( 'Albums', 'wpfab' ),
			'items_list_navigation'      => __( 'Albums list navigation', 'wpfab' ),
			'items_list'                 => __( 'Albums list', 'wpfab' ),
			'most_used'                  => _x( 'Most Used', 'album', 'wpfab' ),
			'back_to_items'              => __( '&larr; Back to Albums', 'wpfab' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'album',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'album_init' );

/**
 * Sets the post updated messages for the `album` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `album` taxonomy.
 */
function album_updated_messages( $messages ) {

	$messages['album'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Album added.', 'wpfab' ),
		2 => __( 'Album deleted.', 'wpfab' ),
		3 => __( 'Album updated.', 'wpfab' ),
		4 => __( 'Album not added.', 'wpfab' ),
		5 => __( 'Album not updated.', 'wpfab' ),
		6 => __( 'Albums deleted.', 'wpfab' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'album_updated_messages' );
