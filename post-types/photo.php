<?php

/**
 * Registers the `photo` post type.
 */
function photo_init()
{
	register_post_type('photo', array(
		'labels'                => array(
			'name'                  => __('Photos', 'wpfab'),
			'singular_name'         => __('Photo', 'wpfab'),
			'all_items'             => __('All Photos', 'wpfab'),
			'archives'              => __('Photo Archives', 'wpfab'),
			'attributes'            => __('Photo Attributes', 'wpfab'),
			'insert_into_item'      => __('Insert into Photo', 'wpfab'),
			'uploaded_to_this_item' => __('Uploaded to this Photo', 'wpfab'),
			'featured_image'        => _x('Featured Image', 'photo', 'wpfab'),
			'set_featured_image'    => _x('Set featured image', 'photo', 'wpfab'),
			'remove_featured_image' => _x('Remove featured image', 'photo', 'wpfab'),
			'use_featured_image'    => _x('Use as featured image', 'photo', 'wpfab'),
			'filter_items_list'     => __('Filter Photos list', 'wpfab'),
			'items_list_navigation' => __('Photos list navigation', 'wpfab'),
			'items_list'            => __('Photos list', 'wpfab'),
			'new_item'              => __('New Photo', 'wpfab'),
			'add_new'               => __('Add New', 'wpfab'),
			'add_new_item'          => __('Add New Photo', 'wpfab'),
			'edit_item'             => __('Edit Photo', 'wpfab'),
			'view_item'             => __('View Photo', 'wpfab'),
			'view_items'            => __('View Photos', 'wpfab'),
			'search_items'          => __('Search Photos', 'wpfab'),
			'not_found'             => __('No Photos found', 'wpfab'),
			'not_found_in_trash'    => __('No Photos found in trash', 'wpfab'),
			'parent_item_colon'     => __('Parent Photo:', 'wpfab'),
			'menu_name'             => __('Photos', 'wpfab'),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array('title', 'editor', 'thumbnail', 'custom-fields'),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_icon'             => 'dashicons-images-alt',
		'show_in_rest'          => true,
		'rest_base'             => 'photo',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	));
}
add_action('init', 'photo_init');

/**
 * Sets the post updated messages for the `photo` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `photo` post type.
 */
function photo_updated_messages($messages)
{
	global $post;

	$permalink = get_permalink($post);

	$messages['photo'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf(__('Photo updated. <a target="_blank" href="%s">View Photo</a>', 'wpfab'), esc_url($permalink)),
		2  => __('Custom field updated.', 'wpfab'),
		3  => __('Custom field deleted.', 'wpfab'),
		4  => __('Photo updated.', 'wpfab'),
		/* translators: %s: date and time of the revision */
		5  => isset($_GET['revision']) ? sprintf(__('Photo restored to revision from %s', 'wpfab'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
		/* translators: %s: post permalink */
		6  => sprintf(__('Photo published. <a href="%s">View Photo</a>', 'wpfab'), esc_url($permalink)),
		7  => __('Photo saved.', 'wpfab'),
		/* translators: %s: post permalink */
		8  => sprintf(__('Photo submitted. <a target="_blank" href="%s">Preview Photo</a>', 'wpfab'), esc_url(add_query_arg('preview', 'true', $permalink))),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf(
			__('Photo scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Photo</a>', 'wpfab'),
			date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)),
			esc_url($permalink)
		),
		/* translators: %s: post permalink */
		10 => sprintf(__('Photo draft updated. <a target="_blank" href="%s">Preview Photo</a>', 'wpfab'), esc_url(add_query_arg('preview', 'true', $permalink))),
	);

	return $messages;
}
add_filter('post_updated_messages', 'photo_updated_messages');
