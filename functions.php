<?php
/**
 * Wpfab Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wpfab
 */

add_action( 'wp_enqueue_scripts', 'twentyseventeen_parent_theme_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function twentyseventeen_parent_theme_enqueue_styles() {
	wp_enqueue_style( 'twentyseventeen-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'wpfab-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'twentyseventeen-style' )
	);

}
include('post-types/photo.php');
include('taxonomies/album.php');

// Show posts of 'post', 'page' and 'photo' post types on home page
function add_my_post_types_to_query( $query ) {
	if ( is_home() && $query->is_main_query() )
	  $query->set( 'post_type', array( 'post', 'page', 'photo' ) );
	return $query;
  }
  add_action( 'pre_get_posts', 'add_my_post_types_to_query' );